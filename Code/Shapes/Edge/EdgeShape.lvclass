﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">GraphBuilder.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../GraphBuilder.lvlib</Property>
	<Property Name="NI.Lib.Description" Type="Str">-----------------------------------------------------------------------
BSD 3-Clause License

Copyright (c) 2020, Cyril GAMBINI
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">EdgeShape</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"./5F.31QU+!!.-6E.$4%*76Q!!$PA!!!27!!!!)!!!$NA!!!!J!!!!!B*(=G&amp;Q;%*V;7RE:8)O&lt;(:M;7)2272H:6.I98"F,GRW9WRB=X-!!!!!!!#A)!#!!!!Q!!!I!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!0I&gt;LTF2\V*,NP5.,T2V##M!!!!-!!!!%!!!!!#L^^";CB'`2:OJS@L-/*J=V"W-W9]!MA4JA!G9\0B#@A!!!!!!!!!!K5`#NT8.A5#/)O"U&gt;B-H*A%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$H9QD1CX[J)6T7WCL2:&amp;RJ!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)A!!!"RYH'.A9W"K9,D!!-3-1-T5Q01$S0Y!YD-!!'A"#$9!!!!!!%5!!!%9?*RD9-!%`Y%!3$%S-$#&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=KF[1@]"_1A&amp;M7-Q'!(&lt;V+"5!!!!!!!!-!!&amp;73524!!!!!!!$!!!"1Q!!!JRYH(.A:'$).,9Q;Q$3T%#MQ.$!E*S@EML&amp;!/1T1%!,%Q0&amp;)!"KHB;;O/'"QWF!I-=PXQ,G&gt;\OIM$48K0!QF@,^,V(B#(A"%GQ_QH'YWS0HO+-.7!F(&amp;E-71]$`Q)TG)TRAX=D[@629$!]U6#ITF!I@&lt;T2BB.A3#$7'Z@!/E-BB60O!?DA/0G4JH1A7[!32H3'-%M&gt;&gt;'(8%A/R?(M:!B$P1\!PL"LISCT%-*N`.&gt;NR"!]1_\C!#I4)A6!7%+A"2/U"%X'%1O@&lt;VP6WA='&amp;$#B-(+'Y!959%VG.A:!!ZHQE)1;&lt;__@``PQV1B!EKJAA6!\&amp;81NE;3(J/1M5=E/Q"[17:U!OE.;$MS6"W!^2&gt;),'L1,I!SLY$D5]1_T&amp;5(9D^#EA,1.E@A@1"+0M&lt;F.U!D1VUWNH@R25Z4=$3'Q$`F'K1!!!!!!YA!9!(!!!'-D!O-#YR!!!!!!!!$#!!A!!!!!1S-#YQ!!!!!!YA!9!(!!!'-D!O-#YR!!!!!!!!$#!!A!!!!!1S-#YQ!!!!!!YA!9!(!!!'-D!O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!O&lt;E!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!O&gt;(&amp;S^'Z!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!O&gt;(&amp;P\_`P]P2O1!!!!!!!!!!!!!!!!!!!!$``Q!!O&gt;(&amp;P\_`P\_`P\`,U&lt;E!!!!!!!!!!!!!!!!!!0``!-P&amp;P\_`P\_`P\_`P\_`S^%!!!!!!!!!!!!!!!!!``]!R=7`P\_`P\_`P\_`P\``SQ!!!!!!!!!!!!!!!!$``Q$&amp;S]P&amp;P\_`P\_`P\`````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,R&lt;_`P\```````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]82````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!S]P,S]P,S]P````````,SQ!!!!!!!!!!!!!!!!$``Q!!R=8,S]P,S``````,U=5!!!!!!!!!!!!!!!!!!0``!!!!!-8,S]P,```,S]5!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!$&amp;S]P,S\]!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!R&lt;]!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!!%!!!!!!!!!H]!!!4J?*SNF%VI%U%5R^_%L5R#C\/RN6VI3#T&lt;7-3!+'I.@B1\&amp;;KF+,(A)&gt;1%NXZ!WWA_R&amp;.\791=?D'9A^"&lt;#(DS%)JX#6\W9%]+^B!&lt;]/[F+.D._H&lt;3X8Q5Y]6&gt;'*:F@O``ZPX`$)$UH9VY'L"G!G&amp;\_$&amp;HAE]T#%!V1O(A#&lt;]%NE"_!RF5C!F4&gt;)(N?"JEV)2_T1D4-TQ00X#X^=V[";]*9&lt;OY^1B4M*D0B+/;-3T@6'N-@4?KZPO=KD)%W!:J?/[JQ:^U1U_B)/A4^CJ(3!-)0S&amp;*_NDNZ/K3LNJ`P2'KC**?%RAX"N*K\3272/E0IC2:B"**/35"3UZ!J6*J18)4#IMW*GWGB'7XS')0RM_.1%;NH2;-4T#IM_8I]'#D90&gt;O1^XI-7YQ2*&amp;\Y2SZZ'DRI$EFO(K^DBSO"^RD%Q&lt;6WH7KU&amp;XN;^`!(@U^%#$6_^4[9LWV?8H?&gt;E(M^;-2YV(#9PA&gt;-_'5&lt;HA34(+U=F!1.EC/$:&gt;N'WY)'\Q]T[$F1_%@0H#J/B;98MZFMEPJ5/JB[-&amp;S-J-*05U`?:\-,I7U:$:ZW+%LX&amp;$/W;?XR51Y9!AE?!;*^GGHI&amp;QOYQ"Q&lt;;&amp;8%2V7;SYHC^.I\M3VVM2NV&gt;&lt;ELO(E_+_\D_TJD5=^4G;BF&gt;;TG&amp;;:&gt;4@5E&gt;&lt;T`T_N&amp;T"%;VVJB4C_+TW3&gt;\%*N;5VDN1+R(MQE]CM&gt;[26K$A[P&gt;*[[8";YY[7G^:CM&gt;D"Y=SD&lt;FIF1JJJV&lt;?N@7M@-YPDF3!"#:&amp;:]-[X&gt;5ZX0O&lt;=SY8'6'Z2Z?]&lt;W!S+L:MQR+@^H^AM6OOHMX3'&lt;;+L?"U&gt;JZNUL`L:P8[K=S[J&lt;]-KO;7_#&gt;+20Z8^:T!!!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!!%!!!!!!!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!)(!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!"#)!#!!!!!!!%!#!!Q`````Q!"!!!!!!!G!!!!!1!?1&amp;!!!":(=G&amp;Q;%*V;7RE:8)[272H:6.I98"F!!!"!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!*3!!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'3!!A!!!!!!"!!5!"Q!!!1!!WPV6UQ!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:)!#!!!!!!!%!"1!(!!!"!!$;`684!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!$QA!)!!!!!!!1!)!$$`````!!%!!!!!!#!!!!!"!"B!5!!!%56E:W64;'&amp;Q:3ZM&gt;G.M98.T!!%!!!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'3!!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!54EEO4&amp;9O17RM,F.P&gt;8*D:5^O&lt;(E!!!!6)!#!!!!!!!%!"!!B!!%!!!%!!!!!!!!!!!1!!A!)!!!!"!!!!%!!!!!I!!!!!A!!"!!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/I!!!&amp;G?*S.D\V/!T%1B$`(A@Q23%+.Z)+#CI98/)'!.A,29][_Z#2,&gt;\LT2:3])RV012Y!C=UF51I;..*K:X:XRA&lt;/36F``&lt;R]!HLW7.FS?&gt;PEQ@HK/KR#`D;^&gt;QP`P,3F&amp;ZY'7^@DAZ,'Q&amp;5SBYO\U.426[&lt;)4,NESCJ@W?C.M^&amp;S*'#)!`7.%K*XF"NJ"UE0H95&amp;`?2$&amp;U^R+`AC9S,?7N;0[?H':1QX74K_/\G?U[(0A-N`Z8&gt;I&lt;;1IZ/R6ILM]C)HBB,')'ZR+W"ZKB\`+98,79PM2R51=E1=B96V22US:32X^!OA@0)5!!!!!!'5!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$:!.1!!!"2!!]%!!!!!!]!W1$5!!!!7A!0"!!!!!!0!.E!V!!!!'/!!)1!A!!!$Q$:!.1)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-A!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!!\Y!!!%6A!!!#!!!!\9!!!!!!!!!!!!!!!A!!!!.!!!"%A!!!!&gt;4%F#4A!!!!!!!!&amp;M4&amp;:45A!!!!!!!!'!5F242Q!!!!!!!!'51U.46!!!!!!!!!'I4%FW;1!!!!!!!!']1U^/5!!!!!!!!!(16%UY-!!!!!!!!!(E2%:%5Q!!!!!!!!(Y4%FE=Q!!!!!!!!)-6EF$2!!!!!!!!!)A&gt;G6S=Q!!!!1!!!)U5U.45A!!!!!!!!+92U.15A!!!!!!!!+M35.04A!!!!!!!!,!;7.M/!!!!!!!!!,54%FG=!!!!!!!!!,I2F"&amp;?!!!!!!!!!,]2F")9A!!!!!!!!-12F"421!!!!!!!!-E6F"%5!!!!!!!!!-Y4%FC:!!!!!!!!!.-1E2&amp;?!!!!!!!!!.A1E2)9A!!!!!!!!.U1E2421!!!!!!!!/)6EF55Q!!!!!!!!/=2&amp;2)5!!!!!!!!!/Q466*2!!!!!!!!!0%3%F46!!!!!!!!!096E.55!!!!!!!!!0M2F2"1A!!!!!!!!1!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!$Q!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!0````]!!!!!!!!"*!!!!!!!!!!!`````Q!!!!!!!!&amp;-!!!!!!!!!!$`````!!!!!!!!!:A!!!!!!!!!!0````]!!!!!!!!"K!!!!!!!!!!%`````Q!!!!!!!!,Q!!!!!!!!!!@`````!!!!!!!!!Q1!!!!!!!!!#0````]!!!!!!!!$&amp;!!!!!!!!!!*`````Q!!!!!!!!-I!!!!!!!!!!L`````!!!!!!!!!TA!!!!!!!!!!0````]!!!!!!!!$4!!!!!!!!!!!`````Q!!!!!!!!.E!!!!!!!!!!$`````!!!!!!!!!XA!!!!!!!!!!0````]!!!!!!!!$`!!!!!!!!!!!`````Q!!!!!!!!A!!!!!!!!!!!$`````!!!!!!!!#"!!!!!!!!!!!0````]!!!!!!!!)'!!!!!!!!!!!`````Q!!!!!!!!K=!!!!!!!!!!$`````!!!!!!!!#K1!!!!!!!!!!0````]!!!!!!!!+L!!!!!!!!!!!`````Q!!!!!!!!K]!!!!!!!!!!$`````!!!!!!!!#M1!!!!!!!!!!0````]!!!!!!!!,,!!!!!!!!!!!`````Q!!!!!!!!MU!!!!!!!!!!$`````!!!!!!!!$5!!!!!!!!!!!0````]!!!!!!!!.3!!!!!!!!!!!`````Q!!!!!!!!V1!!!!!!!!!!$`````!!!!!!!!$8Q!!!!!!!!!A0````]!!!!!!!!/&lt;!!!!!!.272H:6.I98"F,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B*(=G&amp;Q;%*V;7RE:8)O&lt;(:M;7)2272H:6.I98"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!!!"!!%!!!!!!!!!!!!!!1!91&amp;!!!"&amp;&amp;:'&gt;F5WBB='5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;1#!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="EdgeShape.ctl" Type="Class Private Data" URL="EdgeShape.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</LVClass>
