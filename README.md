# Graph Builder

![](Documentation/Logos/GraphBuilder.png)

LabVIEW library to create directed graphs, perform traversals and run graph-related algorithms, and render them in different formats.

## Documentation

Source code developped with LabVIEW 2020.

This library allows you to add "Nodes" (aka vertices) and "Edges".
Nodes and Edges are classes which can be extended (inherit from) to add properties and therefore capabilities.

Each node must be created before linking them with edges.

### Traversals

### Algorithms

- Find root node : Allows finding the root node(s) of a graph. A root node is a node without incoming edges.

- Is Acyclic : Allows determining if a graph contains cycles (closed path) . A cycle is defined as node being linked to itself or non-empty directed trail in which only the first and last vertices are equal.

### Rendering

<u>Rendering formats currently supported are:</u>

* Graphml
* DOT
  * Graphviz

## Licensing

This library is distributed under **BSD-3-Clause License**.
See LICENSE file for details.

## Credits

This library uses [Caraya](https://github.com/JKISoftware/Caraya) to perform unit tests.

## Participating

To participate into the project, please follow these steps :

1. Fork the project

2. Add your code in your project

3. Create a test case to prove your code is functionnal and demonstrate how to use

4. Create a pull request

Wihout test case (made with Caraya), the pull request will not be considered and immediately rejected.

## Supporting

If you want to support the author and the work done here, you can buy me a coffe ;-) https://www.buymeacoffee.com/cyrilg
